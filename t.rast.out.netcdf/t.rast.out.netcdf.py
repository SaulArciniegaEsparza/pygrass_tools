#!/usr/bin/env python

"""
MODULE:    t.rast.out.netcdf

AUTHOR:
    Saul Arciniega-Esparza
    Institute of Engineering of UNAM
    Mexico City, Mexico

PURPOSE:
    Exports a strds to a netcdf file using netCDF4


COPYRIGHT: (C) 018 the authors
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

#%module
#% description: Export strds as netcdf
#% keyword: strds
#% keyword: netcdf
#% keyword: export
#%end

#%option G_OPT_STDS_INPUT
#% key: strds
#% description: Select a strds
#% required: yes
#% multiple: no
#%end

#%option
#% key: tsplit
#% type: string
#% label: Split output files by
#% options: none, day, month, year
#% required: yes
#% answer: none
#%end

#%option G_OPT_F_OUTPUT
#% key: output
#% label: Name of the output netcdf file
#% required: yes
#%end

#%option
#% key: vname
#% type: string
#% label: Output variable name
#% required: no
#% answer: data
#%end

#%option
#% key: units
#% type: string
#% label: Output variable units
#% required: no
#% answer: mm
#%end

#%option
#% key: nodata
#% type: double
#% label: No data value
#% required: no
#% answer: -9999
#%end

#%option
#% key: description
#% type: string
#% label: Netcdf description
#% required: no
#% answer: Space time raster dataset
#%end

#%option
#% key: source
#% type: string
#% label: Netcdf file source
#% required: no
#% answer: Created with GRASS
#%end

#%option
#% key: cstep
#% type: string
#% label: Calendar time step
#% required: no
#% options: hours, days
#% answer: days
#%end

#%option
#% key: cstart
#% type: string
#% label: Start calendar date
#% required: no
#% answer: 1990-01-01
#%end

#%option G_OPT_T_WHERE
#% key: twhere
#%end


# Import modules
import sys
import os

import numpy as np
import pandas as pd

import grass.script as gscript
from grass.pygrass.raster import RasterRow

from netCDF4 import Dataset, date2num


# Initialize the temporal library
import grass.temporal as tgis
tgis.init()


# Define frequency
FREQ = {
    'day': 'D',
    'month': 'M',
    'year': 'Y'
}


# Define sub-functions

def raster2numpy(rastname, mapset=''):
    """
    Return a numpy array from a raster map
    """
    with RasterRow(rastname, mapset=mapset, mode='r') as rast:
        return np.array(rast)


def write_netcdf(saveas, data, time, lon, lat, varname='data', description='', source='', units='',
                 nodata=-9999, calendar_step='days', calendar_start='1990-01-01'):
    """
    Writes a netcdf file from numpy arrays

    :param saveas:
            (string)    output filename
    :param data:
            (array)     3 dimensional array to write as netcdf data
    :param time:
            (datetime array)  output array of dates
    :param lon:
            (array)     1 dimensional longitude array
    :param lat:
            (array)     1 dimensional latitude array
    :param varname:
            (string)    output variable name. BY default 'data'
    :param description:
            (string)    file description information
    :param source:
            (string)    file source information
    :param units:
            (string)    variable units
    :param nodata:
            (float)     variable no data. By default -9999
    :param calendar_step:
            (string)    calendar time step
    :param calendar_start:
            (string)    calendar start time
    """

    # Open file
    fout = Dataset(saveas, 'w', format='NETCDF3_64BIT_OFFSET')

    # Add file descriptions
    fout.description = description
    fout.source = source

    # Add dimensions
    fout.createDimension('time', None)
    fout.createDimension('lat', len(lat))
    fout.createDimension('lon', len(lon))

    # Add longitude and latitude variables
    lat_var = fout.createVariable('lat', 'f8', ('lat',))
    lon_var = fout.createVariable('lon', 'f8', ('lon',))

    lat_var.units = 'degree_noth'
    lon_var.units = 'degree_east'

    lat_var[:] = np.array(lat, dtype=np.float64)
    lon_var[:] = np.array(lon, dtype=np.float64)

    # Add time variable
    time_var = fout.createVariable('time', 'f8', ('time',))
    time_var.units = '{} since {}'.format(calendar_step, calendar_start)
    time_var.calendar = 'standard'
    time_var[:] = date2num(time, units=time_var.units, calendar=time_var.calendar)

    # Create data variable
    data_var = fout.createVariable(varname, 'f8', ('time', 'lat', 'lon'), fill_value=nodata,
                                   least_significant_digit=6)
    data_var.units = units
    data_var.long_name = '{} in {}'.format(varname, units)
    data_var[:, :, :] = data

    # Close file
    fout.close()


def date_fmt(basename, date, step='day'):
    """
    Concatenate a file name with date using a split option
    """

    if step == 'day':
        name = basename + date.strftime('.%Y.%m.%d') + '.nc'
    elif step == 'month':
        name = basename + date.strftime('.%Y.%m') + '.nc'
    elif step == 'year':
        name = basename + date.strftime('.%Y') + '.nc'
    return name


def get_coordinates():
    """
    Returns the longitude and latitude coordinates from the current region
    """

    region = gscript.region()

    lon = np.linspace(
        region['w'] + region['ewres'] / 2.,
        region['e'] - region['ewres'] / 2.,
        region['cols']
    )
    lat = np.linspace(
        region['s'] + region['nsres'] / 2.,
        region['n'] - region['nsres'] / 2.,
        region['rows']
    )

    return lon, lat


def strds_info(strds, twhere):
    """
    Extracts rasters and time from a srtds as DataFrame
    """

    # Connect with time serie
    tserie = tgis.open_old_stds(strds, 'strds')

    # get all rasters contained in strds
    rasters = tserie.get_registered_maps(
        columns="name,mapset,start_time",
        where=twhere,
        order="start_time"
    )

    rows = []
    for row in rasters:
        rows.append([row['start_time'], row['name'], row['mapset']])

    df = pd.DataFrame(
        rows,
        columns=['Date', 'Name', 'Mapset']
    )

    df['Date'] = pd.to_datetime(df['Date'])
    df.set_index('Date', inplace=True)

    return df


# Main function

def main():
    # Read parameters
    options, flags = gscript.parser()
    # required parameters
    strds = options['strds']
    tsplit = options['tsplit']
    output = options['output']

    # optionals
    nodata = options['nodata']
    vname = options['vname']
    vunits = options['units']
    desc = options['description']
    source = options['source']
    cstep = options['cstep']
    cstart = options['cstart']
    twhere = options['twhere']

    # Load strds elements
    df_strds = strds_info(strds, twhere)

    # Get longitude and latitude
    lon, lat = get_coordinates()

    # Store all data in a single netcdf
    if tsplit.lower() == 'none':
        # build empty matrix
        n = len(df_strds)
        nr, nc = len(lat), len(lon)
        outdata = np.full((n, nr, nc), np.nan, dtype=float)

        # read rasters
        for i in range(n):
            outdata[i, :, :] = np.flipud(raster2numpy(df_strds.iloc[i, 0],
                                                      mapset=df_strds.iloc[i, 1]))

        # save output netcdf
        if not output.lower().endswith('.nc'):
            output += '.nc'
        write_netcdf(
            output,
            outdata,
            [x.to_pydatetime() for x in df_strds.index],
            lon,
            lat,
            varname=vname,
            description=desc,
            source=source,
            units=vunits,
            nodata=nodata,
            calendar_step=cstep,
            calendar_start=cstart
        )

    else:
        basepath = os.path.dirname(output)
        basename = os.path.splitext(os.path.basename(output))[0]

        for basetime, df in df_strds.groupby(pd.Grouper(freq=FREQ[tsplit])):
            # build empty matrix
            n = len(df)
            nr, nc = len(lat), len(lon)
            outdata = np.full((n, nr, nc), np.nan, dtype=float)

            # read rasters
            for i in range(n):
                outdata[i, :, :] = np.flipud(raster2numpy(df.iloc[i, 0],
                                                          mapset=df.iloc[i, 1]))

            # output filename
            saveas = os.path.join(
                     basepath,
                     date_fmt(basename, basetime, step=tsplit)
            )

            # save output netcdf
            write_netcdf(
                saveas,
                outdata,
                [x.to_pydatetime() for x in df.index],
                lon,
                lat,
                varname=vname,
                description=desc,
                source=source,
                units=vunits,
                nodata=nodata,
                calendar_step=cstep,
                calendar_start=cstart
            )


if __name__ == '__main__':
    sys.exit(main())
