#!/usr/bin/env python

"""
MODULE:    t.rast.in.netcdf

AUTHOR:
    Saul Arciniega-Esparza
    Institute of Engineering of UNAM
    Mexico City, Mexico

PURPOSE:
    Imports a netcdf as a strds. strds can be overwrited or appended
    Netcdf file must be well builded with 3dimensions (time, lat, lon)


COPYRIGHT: (C) 018 the authors
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

#%module
#% description: Import netcdf as strds
#% keyword: strds
#% keyword: netcdf
#% keyword: import
#%end

#%option G_OPT_F_INPUT
#% key: filename
#% label: Input netcdf file (.nc or .nc4) or file list
#% required: yes
#% multiple: yes
#%end

#%option
#% key: varname
#% type: string
#% label: Variable name contained in netcdf
#% required: yes
#% answer: varname
#%end

#%option
#% key: basename
#% type: string
#% label: Prefix for output rasters
#% required: yes
#% answer: varname
#%end

#%option G_OPT_STDS_INPUT
#% key: strds
#% description: Output strds name to register maps
#% required: yes
#% multiple: no
#%end

#%flag
#% key: r
#% description: Rotate dimensions from (lon,lat) to (lat,lon)
#%end

#%flag
#% key: c
#% description: Fix longitude (0,360) to (-180,180)
#%end

#%option
#% key: calendar
#% type: string
#% options: none, standard, gregorian
#% label: Calendar used for time calculations (only if not defined as attribute)
#% required: no
#% answer: none
#%end

#%option
#% key: endstep
#% type: string
#% label: Time step to generate the end_time of rasters (NNN seconds, minutes, hours, days, months, years)
#% required: no
#%end

#%option
#% key: layer
#% type: integer
#% label: Layer to extract only for 4D variables (time,layer,lat,lon) or (time,layer,lon,lat)
#% required: no
#% answer: 1
#%end


# Import modules
import sys
import os

import numpy as np
import pandas as pd

import grass.script as gscript
from grass.pygrass.raster import RasterRow
from grass.pygrass.raster.buffer import Buffer

from netCDF4 import Dataset, num2date


# Initialize the temporal library
import grass.temporal as tgis
tgis.init()


# Define functions

def array2raster(array, mtype, rastname, overwrite=False):
    """Save a numpy array to a raster map"""
    with RasterRow(rastname, mode='w', mtype=mtype, overwrite=overwrite) as new:
        newrow = Buffer((array.shape[1],), mtype=mtype)
        for row in array:
            newrow[:] = row[:]
            new.put_row(newrow)


def set_region(lon, lat):
    """Set computational region from longitude and latitude arrays"""
    nsres = np.abs(np.diff(lat)).mean()
    ewres = np.abs(np.diff(lon)).mean()

    cols = len(lon)
    rows = len(lat)

    south, north = lat.min()-nsres/2., lat.max()+nsres/2.
    west, east = lon.min()-ewres/2., lon.max()+ewres/2.

    gscript.run_command(
        'g.region',
        n=north,
        s=south,
        e=east,
        w=west,
        rows=rows,
        cols=cols
    )


def get_dim_varnames(nc_connect):
    """
    Returns a dict with the name of the dimension variables time, longitude and latitude
    """
    dim_names = tuple(nc_connect.dimensions.keys())
    var_names = tuple(nc_connect.variables.keys())

    names = {
        'time': {
            'var': None,
            'dim': None,
        },
        'lat': {
            'var': None,
            'dim': None,
        },
        'lon': {
            'var': None,
            'dim': None,
        },
    }

    # Define possible names
    possible_names = {
        'time': ('t', 'T', 'time', 'Time', 'times', 'Times', 'date', 'Date', 'dates', 'Dates'),
        'lat': ('lat', 'Lat', 'latitude', 'Latitude'),
        'lon': ('lon', 'Lon', 'longitude', 'Longitude'),
    }

    # Search variables and dimensions matching
    for keyv in names.keys():
        for key in var_names:
            if key in possible_names[keyv]:
                names[keyv]['var'] = key
                break
        for key in dim_names:
            if key in possible_names[keyv]:
                names[keyv]['dim'] = key
                break

    return names


def get_time(nc_connect, timeprops, calendar='standard', endstep=None):
    """Read time variable from a netcdf as datetimes"""

    # Read time dimension and var
    timev = nc_connect.variables[timeprops['var']]

    try:
        units = timev.units
    except AttributeError:
        raise AttributeError('units attribute was not found in time variable')
    try:
        calendar = timev.calendar
    except AttributeError:
        print('Attribute calendar was not found. Calendar was set as {}'.format(calendar))

    start_time = num2date(timev[:], units=units, calendar=calendar)
    dates = pd.DataFrame(pd.to_datetime(start_time), columns=['start_time'])

    # Set end_time from endstep
    if type(endstep) is str:
        # split endstep
        step, unit = endstep.split(' ')
        step, unit = int(step), unit.lower()

        if unit in ('seconds', 'second'):
            dt = pd.offsets.Second(step)
        elif unit in ('minutes', 'minute'):
            dt = pd.offsets.Minute(step)
        elif unit in ('hours', 'hour'):
            dt = pd.offsets.Hour(step)
        elif unit in ('days', 'day'):
            dt = pd.offsets.Day(step)
        elif unit in ('months', 'month'):
            dt = pd.offsets.MonthEnd(step)
        elif unit in ('years', 'year'):
            dt = pd.offsets.YearEnd(step)
        else:
            raise ValueError('Bad endstep argument {}'.format(endstep))

        dates['end_time'] = dates['start_time'] + dt

    else:
        raise TypeError('endstep {} must be a string'.format(endstep))

    return dates


def get_coors(nc_connect, latprops, lonprops):
    """ Read latitude and longitude coordinates from a netcdf"""

    lat = np.array(nc_connect.variables[latprops['var']][:])
    lon = np.array(nc_connect.variables[lonprops['var']][:])
    return lat, lon


def read_band(nc_connect, varname, index, layer=None,
              rotate=False, lon_index=None, flip=False):
    """Reads a netcdf band using different options"""
    # Read selected variable
    if layer:
        data = nc_connect.variables[varname][index, layer-1, :, :]
    else:
        data = nc_connect.variables[varname][index, :, :]
    values = np.array(data, dtype=np.float32)
    if 'mask' in dir(data):
        values[data.mask] = np.nan
    else:
        fillval = nc_connect.variables[varname]._FillValue
        values[values == fillval] = np.nan
    if rotate:
        values = values.transpose()
    # Invert data to the correct direction
    if flip:
        values = np.flipud(values)
    # Try to convert 0 to 360 to -180 to 180
    if lon_index is not None:
        values = values[:, lon_index]
    return values


def fix_longitude_indexes(lon):
    """Convert longitude from 0,360 to -180,180"""

    index = np.arange(0, len(lon), dtype=int)
    mask = lon > 180
    new_index = np.hstack((index[mask], index[~mask]))
    new_lon = np.hstack((lon[mask]-360, lon[~mask]))

    return new_lon, new_index


def strds_exists(serie):
    """Verify if a strds already exist"""
    tlist = tgis.tlist('strds')
    if tlist:
        return serie in tlist
    else:
        return False


def read_files(filename):

    # Single netcdf file
    if (filename.lower().endswith('.nc')
            or filename.lower().endswith('.nc4')):
        infiles = [filename]

    else:
        infiles = pd.read_csv(filename, header=None)[0].tolist()
    return infiles


def main():
    # Read inputs
    options, flags = gscript.parser()

    infiles = read_files(options['filename'])
    varname = options['varname']
    basename = options['basename']
    strds = options['strds']

    calendar = options['calendar']
    endstep = options['endstep']
    layer = options['layer']

    rotate = flags['r']
    fix_lon = flags['c']

    mapset = gscript.gisenv()['MAPSET']

    # Connect with strds
    print('\nConnecting with raster time dataset')
    if '@' not in strds:
        strds += '@' + mapset

    if strds_exists(strds):
        stds = tgis.open_old_stds(
            strds,
            'strds'
        )
    else:
        stds = tgis.open_new_stds(
            strds,
            'strds',
            'absolute',
            strds.upper(),
            '.',
            'mean',
            overwrite=True
        )

    # Iterate over files
    for filename in infiles:
        print('Reading file {}'.format(filename))

        # Get netcdf data
        nc_connect = Dataset(filename)
        names = get_dim_varnames(nc_connect)
        time = get_time(
            nc_connect,
            names['time'],
            calendar=calendar,
            endstep=endstep)
        lat, lon = get_coors(
            nc_connect,
            names['lat'],
            names['lon'])
        if fix_lon:
            lon, indexes = fix_longitude_indexes(lon)
        else:
            indexes = None
        if lat[0] > lat[-1]:
            flip = False
        else:
            flip = True

        set_region(lon, lat)

        time_str = time['start_time'].dt.strftime('%Y.%m.%d.%H.%M')

        # Check for 4D layers
        if len(nc_connect.variables[varname].dimensions) == 4:
            layer = int(layer)
            if layer < 1:
                layer = 1
            elif layer > nc_connect.variables[varname].shape[1]:
                layer = nc_connect.variables[varname].shape[1]
        else:
            layer = None

        # Read bands
        maps_to_register = []
        for i in range(len(time)):
            data = read_band(
                nc_connect,
                varname,
                i,
                layer=layer,
                rotate=rotate,
                lon_index=indexes,
                flip=flip
            )

            # Write raster
            rastname = '{}.{}@{}'.format(basename, time_str.iloc[i], mapset)
            array2raster(data, 'FCELL', rastname, overwrite=True)

            # create MapDataset
            map_dts = tgis.RasterDataset(rastname)
            map_dts.load()
            if 'end_time' in time:
                map_dts.set_absolute_time(
                    start_time=time['start_time'].iloc[i].to_pydatetime(),
                    end_time=time['end_time'].iloc[i].to_pydatetime()
                )
            else:
                map_dts.set_absolute_time(
                    start_time=time['start_time'].iloc[i].to_pydatetime()
                )

            # Save raster list
            maps_to_register.append(map_dts)

        nc_connect.close()

        # Register maps in strds
        tgis.register.register_map_object_list(
            'raster',
            maps_to_register,
            stds
        )

    # Update dataset
    stds.update_from_registered_maps()
    stds.print_shell_info()


if __name__ == '__main__':
    sys.exit(main())
